-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jul 05, 2018 at 06:11 AM
-- Server version: 10.1.22-MariaDB
-- PHP Version: 7.1.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `business`
--

-- --------------------------------------------------------

--
-- Table structure for table `mdybusiness`
--

CREATE TABLE `mdybusiness` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `phno` varchar(10) NOT NULL,
  `url` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mdybusiness`
--

INSERT INTO `mdybusiness` (`id`, `name`, `address`, `city`, `phno`, `url`) VALUES
(1, 'Mother Mandalay', 'Corner of YGN_MDY,Highway Road', 'Amarapura', '0280865', 'www.mothermandalay.com'),
(2, 'Triple Circle Industry', 'Padamyar Industrial Zone,Sagaing', 'Sagaing', '07222110', 'www.triplecircle.com'),
(3, 'Aung Myanmar Asia', 'Between 50&51 street', 'Mandalay', '092004000', 'www.aungmyanmarasia.com'),
(4, 'Myanmar Unisteel Trading', '63 street,Between 41&42 street', 'Mandalay', '022832563', 'www.myanmarunisteeltrading.com'),
(5, 'Gold Home Family', 'Corner of 62 street,Mdy', 'Mandalay', '0991001438', 'www.goldhomefamily.com'),
(6, 'Glass', 'Corner of 63 street,Mdy', 'Mandalay', '02381672', 'www.glasscompany.com'),
(7, 'Myint Shwe Wah Industry', 'Corner of YGN_MDY,Highway Road', 'Mandalay', '02591349', 'www.myintshwewah.com'),
(8, 'I.E.C.M Company Limited', '82 street,Between 31&32 street', 'Amarapura', '09445566', 'www.iemcompany.com'),
(9, 'Min Dhama Company', '68 street, 33 & 32 street', 'Mandalay', '0214748364', 'www.informindhama.com'),
(10, 'Cherry Yoma', '83 street, 32 and 33 street', 'Mandalay', '0979509039', 'www.cherryyoma.com'),
(11, 'FTP Myanmar ', '88 street , 43 and 42 street ', 'Mandalay', '0928364826', 'www.ftpmyanmar.com'),
(12, 'King Group', '78 street , 28 and 27 street', 'Mandalay', '097875456', 'www.kinggroup.com'),
(13, 'IKBZ Insurrance', 'Corner of 52 street Between 26 and 27', 'Mandalay', '094242333', 'www.ikbzinsur.com'),
(14, 'Distinct Trading', 'Corner of 73 street,Mdy', 'Mandalay', '092436244', 'www.distincttrading.com'),
(15, 'Moonet Group', 'Corner of 77 street,Mdy', 'Mandalay', '0998767141', 'www.moonetgroup.com'),
(16, 'Kamax Group', 'Corner of 44 street,Mdy', 'Mandalay', '0917648252', 'www.kamaxgroup.com'),
(17, 'M9 Group', 'Corner 73x35 street', 'mandalay', '0998829374', 'www.m9group.com'),
(18, 'Lucky Company', '489 Tampawadi,Chanmya Tharzi', 'Mandalay', '02-59034', 'www.luckylucky.com'),
(19, 'Loi Hein Company', 'Chan Aye Thar Zan Township', 'Mandalay', '0973219492', 'www.loihein@gmail.com'),
(20, 'Gold Fuji', 'Kywesekan Qr , Pyigyitagon', 'Mandalay', '092029300', 'www.goldfji@gmail.com'),
(21, 'Shwe Thitsar Company', 'Street 81,bet 29*30 ', 'Mandalay', '092827393', 'www.shweshwethitsar@gmail.com'),
(22, 'Pyae Sone Company', '153, st 74,bet 29*30', 'Mandalay', '0998998830', 'www.pyaesibe@gmail.com'),
(23, 'MCPF Company', 'Street 81,bet 44*45 ', 'Mandalay', '0998282739', 'www.mcpfcoltd@gmail.com'),
(24, 'Myint Company', '4, St35,Bet 61*62', 'Mandalay', '0920000293', 'www.myint@gmail.com'),
(25, 'Madina Family Company', 'No 189, St 82,Bet 21*22 St', ' Mandalay', '02351PP05', 'www.madinafamily@gmail.com'),
(26, 'Jinlong Myanmar Company', 'No 493,St 82,Bet 35*36 St', 'Mandalay', '0221438', 'www.jinlong@gmail.com'),
(27, 'Sweety Home Company', 'No 236,St 33,Bet 81*82 St', 'Mandalay', '0991029989', 'www.sweetyhome@gmail.com'),
(28, 'Silver Tiger Company', 'No 268, St 26,Bet 83*84', 'Mandalay', '0234957', 'www.silvertiger@gmail.com'),
(29, 'Shwe Htun Company', 'No (3-1), St 72 ', 'Mandalay', '0280527', 'www.shwehtun@gmail.com'),
(30, 'Swan Inn Company', 'St 62,Chan Mya Thar Zi', 'Mandalay', '092004375', 'www.swaninn@gmail.com'),
(31, 'VS International', 'St 77,Bet 32*33 St', 'Mandalay', '097864467', 'www.vsinternational@gmail.com'),
(32, 'CZM Company', 'St 66, YawMinGyin Quarter', 'Mandalay', '025154403', 'www.cancom@gmail.com'),
(33, 'Billion Gain Company', 'St 36,Bt 81*82 ,Maharaungmyay Township', 'Mandalay', '0271824', 'www.billiongain@gmail.com'),
(34, 'Triple Circle Company', 'No 784,Bt 62*63 St', ' Mandalay', '0272965', 'www.triplecircle@gmail.com'),
(35, 'Myanmar Central Trading Company', 'St 64,Bet 41*42 St', 'Mandalay', '0943063369', 'www.myanmarcentraltt@gmail.com'),
(36, 'Shwete Company', 'No.552,St 80,Bet 33*34 St', 'Mandalay', '02378195', 'www.shwetem@gmail.com'),
(37, 'New Star Light Company', 'Corner of St 29,Chan Aye Thar Zan Township', 'Mandalay', '0224562', 'www.newstar@gmail.com'),
(38, 'Tun Tun Win Company', 'St 61,Bet 34*35 St', 'Mandalay', '025154855', 'www.tuntunwin@gmail.com'),
(39, 'San Thit Oo Company', 'St 12,Aungmyaytharzan Township', 'Mandalay', '0278659', 'www.santhitoo@gmail.com'),
(40, 'New Super Company', 'No 3,Yangon-Mandalay Road', 'Mandalay', '095184594', 'www.newsuper@gmail.com'),
(41, 'Venus Company', 'St 80,Bet 15*16 St', 'Mandalay', '0231124', 'www.venuscom@gmail.com'),
(42, 'Sunlight Company', 'St 80,Corner of 37 St', 'Mandalay', '0991014808', 'www.sunlight@gmail.com'),
(43, 'Myanmar White Elephant Company', 'No.202,St 31,Bet 83*84 St', 'Mandalay', '0973079688', 'www.mmwhite@gmail.com'),
(44, 'YUASA Company', 'St 82,Bet 27*28 St', 'Mandalay ', '0260994', 'www.yuasa@gmail.com'),
(45, 'Honey Company', 'St 83,Corner of 30 St', 'Mandaly', '0232211', 'www.honey@gmail.com'),
(46, 'Yadanar Moe Company ', 'No.54,Aye Chan Thar Zan Township', 'Mandalay', '0233578', 'www.yadanamoecom@gmail.com'),
(47, 'Seagull Company', 'No.80,Bet 31*32 St', 'Mandalay', '0260148', 'www.seagull@gmail.com'),
(48, 'Giorenzo Company', 'St 80,Bet 30*31 St', 'Mandalay', '09980254', 'www.giorenzo@gmail.com'),
(49, 'Golden Lion Company', 'No.104,St 62,Bet 30*31 St', 'Mandalay', '0265585', 'www.goldenlion@gmail.com'),
(50, 'Grand Royal Company', 'St 32,Bet 76*77 St', 'Mandalay', '0269569', 'www.grandroyal@gmail.com'),
(51, 'Mercury Rays Company', 'No.405,Corner of 31 St', 'Mandalay', '09794456', 'www.mercury@gmail.com'),
(52, 'Oo Te Bwar Company', 'No.64,Bet 33*34 St,Bet 85*86 St', 'Mandalay', '092012532', 'www.ootebwr@gmail.com'),
(53, 'Myint Zu Thar Company', 'Bet 69*71 St,Manawhaery Road', 'Mandalay', '0277825', 'myintzu@gmail.com'),
(54, 'Five Hero Company', 'Yangon-Mandaly Road', 'MyintNge', '092005838', 'www.fivehero@gmail.com'),
(55, 'Seven Star Company', 'St 81,Bet 33*34 St', 'Mandalay', '0266435', 'www.sevenhero@gmail.com'),
(56, 'Shwe Lat Shan Company', 'St 62,PyiGyiTaKon Township', 'Mandalay', '0925609462', 'www.shwelatshan@gmail.com'),
(57, 'Sky Blue Company', 'Bet 59*60 St,Bet 32*35 St', 'Mandalay', '0274683', 'www.skyblue@gmail.com'),
(58, 'Global Power Company', 'No.123,Bet 35*36 St', 'Mandalay', '02234667', 'www.globalpower@gmail.com'),
(59, 'Ocean Glory Company', 'St 29,Bet 79*80 St', 'Mandalay', '0236243', 'www.oceanglory@gmail.com'),
(60, 'I love it Company', 'St 78,Bet 34*35 St', 'Mandalay', '09794578', 'www.iloveit@gmail.com'),
(61, 'Shwe Sin Min Company', 'No.302,Bet 81*82 St', 'Mandalay', '02445590', 'www.shwesinmin@gmail.com'),
(62, 'Lucky Lucky Company', 'St 25,Bet 86*87 St', 'Mandalay', '092042087', 'www.luckylucky@gmail.com'),
(63, 'Sein Nan Daw Company', 'st 26,Bet 88*89 St', 'Mandalay', '02208994', 'www.seinnandaw@gmail.com'),
(64, 'Great Wall Company', 'St 84,Bet 38*39 St', 'Mandalay', '0996645', 'www.greatwall@gmail.com'),
(65, 'Myat TawWin Company', 'Yangon-Mandalay Road', 'MyitNge', '02237865', 'www.myattawwin@gmail.com'),
(66, 'Cherry Oo Company', 'St 26,Bet 84*85 St', 'Mandalay', '02218890', 'www.cherryoo@gmail.com'),
(67, 'Great One Company', 'H/222 53 St', 'Amarapura', '025154946', 'www.greatone@gmail.com'),
(68, 'KTM Company', '80 St,Bet 36*37', 'Mandalay', '0276158', 'www.ktmcom@gmail.com'),
(69, 'Three Dragon Company', '22 St,Bet 82*83', ' Mandalay', '0269725', 'www.threedragon@gmail.com'),
(70, 'Technoland Company', '84 St, Bet 32*33', 'Mandalay', '0277435', 'www.technoland@gmail.com'),
(71, 'Ayeyarwady Partners Trading co.,ltd', 'Pyigyitagon Quarter', 'Mandalay', '0940263456', 'www.ayeyarwady@gmail.com'),
(72, 'Royal Star Company', '119/ St23', 'Amarapura', '0232516', 'www.royalstar@gmail.com'),
(73, 'Gold Company', 'No(2/118) Bet 52*53', ' Mandalay', '0991001438', 'www.goldgoldcom@gmail.com'),
(74, 'Mandalay Stars Co.,Ltd', 'St35, Bet80*81', ' Mandalay', '0271878', 'www.mandalaystars@gmail.com'),
(75, 'Aha Company', 'St9, Pyigyitagon Tsp', 'Mandalay', '0259995', 'www.ahacomltd@gmail.com'),
(76, 'Royal White Fruits Production Co.,Ltd', 'St78, Bet30*31', 'Mandalay', '02-35086', 'www.royalwhitefruit.com'),
(77, 'Five Star Company', 'St51', 'Mandalay', '02-5154331', 'www.fivestar.com'),
(78, 'Royal Shinning Star Co.,Ltd', 'St68,Pyigyitagon Tsp', 'Mandalay', '02-5152263', 'www.royalshinningstar.com'),
(79, 'Gold Standard Company', 'St26,Bet90*91', ' Mandalay', '02-66098', 'www.goldstandard.com'),
(80, 'Good Brother Company', 'St81,Bet35*36', 'Mandalay', '02-39948', 'www.goodbrother.com'),
(81, 'Sweety Life Company', 'Bet66*67,Theik Pan St', 'Mandalay', '02-77267', 'www.sweetylife.com'),
(82, 'Wounder House Company', 'St35,Bet77*78', 'Mandalay', '02-60230', 'www.wounderhouse.com'),
(83, 'True Brother Company', 'St16,Bet86*87', ' Mandalay', '09-2031873', 'www.truebrother.com'),
(84, 'Yee Shin Company', 'St80,Bet33*34', 'Mandalay', '02-30880', 'www.yeeshin.com'),
(85, 'Yadanabon Glass Co.Ltd', 'St75, Bet33*34', 'Mandalay', '02-73941', 'www.yadanabonglass.com'),
(86, 'Myanmar Apple Company', 'St61,Bet55*56', 'Mandalay', '09-5415013', 'www.myanmarapple.com'),
(87, 'Sein Thiha Company', 'St61,Ayeyawady Corner', 'Mandalay', '02-5154183', 'www.seinthiha.com'),
(88, 'Royal Flex Company', 'St30,Bet77*78', 'Mandalay', '09-8615271', 'www.royalflex.com'),
(89, 'Triple International Company', 'St81,Bet30*31', 'Mandalay', '09-6502711', 'www.tripleinternational.com'),
(90, 'Triple Nine Company', '64/ Bet33*34,Bet85*86', 'Mandalay', '09-4712587', 'www.triplenine.com'),
(91, 'SMA Co.,Ltd', 'Yangon_Mandalay Highway Road', 'MyitNge', '09-2056718', 'www.smacoltd.com'),
(92, 'Amazing One Company', 'St81,Bet36*37', 'Mandalay', '09-2003788', 'www.amazingone.com'),
(93, 'Vivala Company', 'St42,Bet66*67', 'Mandalay', '09-2024386', 'www.vivala.com'),
(94, 'Zalatwah Company', 'St32,Bet83*84', 'Mandalay', '02-21295', 'www.zalatwah.com'),
(95, 'Golden Lion Company', 'St85,Bet36*37', 'Mandalay', '02-252550', 'www.goldenlion.com'),
(96, 'Hyper Sonic Trading Company', 'St78,Bet51*52', 'Mandalay', '02-501265', 'www.hypersonic.com'),
(97, 'New World Commpany', 'St83,Bet23*24', 'Mandalay', '09-7889573', 'www.newworldcompany.com'),
(98, 'Phado Company', 'St75,Bet42*43', 'Mandalay', '02-5154566', 'www.phadocompany.com'),
(99, 'Shwe Company', 'St64,Bet71*72', 'Mandalay', '09-7989563', 'www.shwecompany.com'),
(100, 'Charles & Keth', 'St69,Bet36*37', 'Mandalay', '02-501326', 'www.charlesketh.com'),
(101, 'Michael Cors Company', 'St73,Bet63*64', 'Mandalay', '09-5021654', 'www.michaelcors.com'),
(102, 'Bwin Company', 'St82,Bet41*42', 'Mandalay', '02-512364', 'www.bwincompany.com'),
(103, 'Billion Gain Manufacturing Co.,Ltd', 'St36,Bet81*82', 'Mandalay', '02-71824', 'www.billiongainmanufacturing.com'),
(104, 'Nataional Company', 'St80,Bet28*29', 'Mandalay', '02-341104', 'www.nataional.com'),
(105, 'Shwete Company', 'St80,Bet33*34', 'Mandalay', '02-39103', 'www.shwetecom.com'),
(106, 'New Super Indudtrial Company', 'St64,Beet34*34', 'Mandalay', '951-400136', 'www.newsuperindudtrial.com');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `mdybusiness`
--
ALTER TABLE `mdybusiness`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `mdybusiness`
--
ALTER TABLE `mdybusiness`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=107;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
